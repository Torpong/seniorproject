class Student < ApplicationRecord
  include AASM
  has_many :students_posts
  has_many :recommendations
  has_many :comments, :dependent => :destroy
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "http://via.placeholder.com/150x150"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  ActsAsTaggableOn.delimiter = [',', ' ']
  ratyrate_rater
  ratyrate_rateable "score"
  acts_as_taggable_on :skills


  aasm :column => 'state' do # default column: aasm_state
    state :no_request, :initial => true
    state :requested

    event :to_requested do
      transitions :from => :no_request, :to => :requested
    end

    event :to_filled do
      transitions :from => :requested, :to => :no_request
    end

  end
end
