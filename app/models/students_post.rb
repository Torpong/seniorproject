class StudentsPost < ApplicationRecord
  include AASM
  belongs_to :student
  belongs_to :post

  aasm :column => 'status' do # default column: aasm_state
    state :waiting, :initial => true
    state :accepted_interview
    state :offered_internship
    state :rejected
    state :accepted

    event :to_interview do
      transitions :from => :waiting, :to => :accepted_interview
    end

    event :to_internship do
      transitions :from => :accepted_interview, :to => :offered_internship
    end

    event :to_reject do
      transitions :from => [:accepted_interview, :waiting, :offered_internship], :to => :rejected
    end

    event :to_accept do
      transitions :from => :offered_internship, :to => :accepted
    end

  end

  def self.automail
    mg_client = Mailgun::Client.new 'key-1f0ceaa138d186ae2a34508c5ea0fbe7'
    message_params =  { from: 'bob@sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org',
                        to:   'torpong.boss@gmail.com',
                        subject: 'Interview from loaclhost',
                        text:    'You have been selected for Interview in xxx company post'
                      }
    mg_client.send_message 'sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org', message_params
  end

  def self.acceptmail
    mg_client = Mailgun::Client.new 'key-1f0ceaa138d186ae2a34508c5ea0fbe7'
    message_params =  { from: 'bob@sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org',
                        to:   'torpong.boss@gmail.com',
                        subject: 'Interview from loaclhost',
                        text:    'You have been accepted in xxx company post'
                      }
    mg_client.send_message 'sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org', message_params
  end

end
