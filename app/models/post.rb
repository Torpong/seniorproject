class Post < ApplicationRecord
  include AASM
  belongs_to :company
  has_many :students_posts
  acts_as_taggable_on :tags
  before_create :set_created_at


  # scope :status, -> (status) { where status: status }
  # scope :location, -> (location_id) { where location_id: location_id }
  # scope :tagged_with, -> (tagName) {Post.tagged_with([tagName], :any => true)}
  # scope :tagged_with, -> (owner) { tagged_with(owner) }
  scope :is_opened, -> { where(state: "opened") }
  scope :starts_with, -> (title) { where("REPLACE (lower(title), ' ', '' ) like ?", "%#{title.downcase}%") }
  scope :has_tag, -> (title) { Post.tagged_with(title) }
  scope :is_published, -> { where(publish: "true") }
  # scope :starts_with, -> (title) { where("title like ?", "#{title}%") + Post.tagged_with(title)}
  aasm :column => 'state' do # default column: aasm_state
    state :opened, :initial => true
    state :closed
    state :completed

    event :to_closed do
      transitions :from => :opened, :to => :closed
    end

    event :to_completed do
      transitions :from => :closed, :to => :completed
    end

  end



  def self.check
    @post = Post.all
    @post.each do |post|
      if post.opened?
        if post.finished_at < Time.now
          post.to_closed!
        end
      end
    end
  end
  private
  def set_created_at
    self.created_at = Time.now
  end
end
