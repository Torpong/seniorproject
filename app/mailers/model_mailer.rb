class ModelMailer < ApplicationMailer
  default from: "me@sandbox398f07667a2e474c9c87a8fe345516a9.org"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.model_mailer.new_record_notification.subject
  #
  def new_record_notification(record)
    @record = record
    mail to: "Torpong.boss@gmail.com", subject: "Success! You did it."
  end
end
