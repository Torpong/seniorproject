class RecommendationsController < ApplicationController

  # GET /recommendations
  def index
    @recommendations = Recommendation.all
  end
end
