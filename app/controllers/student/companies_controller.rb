class Student::CompaniesController < Student::AppController
  before_action :set_company, only: [:show]


  # GET /companies/1
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

end
