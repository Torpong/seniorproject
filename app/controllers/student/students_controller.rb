class Student::StudentsController < Student::AppController
  before_action :set_student, only: [:show, :edit, :update, :destroy, :status]

  # GET /students
  def index
    @students = Student.all
  end

  # GET /students/1
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # GET /students/status
  def status
  end

  # POST /students
  def create
    @student = Student.new(student_params)

    if @student.save
      flash[:success] =  'Student was successfully created.'
      redirect_to [:student, @student]
    else
      render :new
    end
  end

  # PATCH/PUT /students/1
  def update
    student_params[:password] = current_student.password
    if @student.update(student_params)
      flash[:warning] = 'Student was successfully updated.'
      redirect_to [:student, @student]
    else
      render :edit
    end
    # if current_student.update_with_password(params[:password])
    #   sign_in(current_student, :bypass => true)
    #   flash[:notice] = 'Password updated.'
    #   redirect_to account_path
    # else
    #   render :action => :show
    # end
  end

  # DELETE /students/1
  def destroy
    @student.destroy
    flash[:danger] = 'Student was successfully destroyed.'
    redirect_to student_students_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def student_params
      params.require(:student).permit(:name, :age, :occupation, :email, :skill_list,:avatar)
    end

end
