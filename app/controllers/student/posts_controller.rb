class Student::PostsController < Student::AppController
  require 'will_paginate/array'
  before_action :set_post, only: [:show]
  has_scope :starts_with, :has_tag, :is_opened, :is_published

  def index
      @posts = apply_scopes(Post.all).paginate(:page => params[:page], :per_page => 5)
      @posts = apply_scopes(@posts).is_opened
      @posts = apply_scopes(@posts).is_published
  end

  # GET /posts/1
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title, :state, :jobDescription, :salary,
                                    :skillRequirement, :location, :company_id,
                                     :tag_list)
    end
end
