class Student::AppController < ApplicationController
  before_action :authenticate_student!, except: [:show]
end
