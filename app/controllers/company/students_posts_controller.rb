class Company::StudentsPostsController < Company::AppController
  before_action :set_students_post, only: [:show, :edit, :update, :destroy, :interview, :internship]
  require 'mailgun'

  # GET /students_posts
  def index
    @students_posts = StudentsPost.all
  end

  # GET /students_posts/1
  def show
  end

  # GET /students_posts/new
  def new
    @students_post = StudentsPost.new
  end

  # GET /students_posts/1/edit
  def edit
  end

  # POST /students_posts
  def create
# # First, instantiate the Mailgun Client with your API key
# mg_client = Mailgun::Client.new 'key-1f0ceaa138d186ae2a34508c5ea0fbe7'
#
# # Define your message parameters
# message_params =  { from: 'bob@sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org',
#                     to:   'torpong.boss@gmail.com',
#                     subject: 'The Ruby SDK is awesome!',
#                     text:    'It is really easy to send a message!'
#                   }
#
# # Send your message through the client
# mg_client.send_message 'sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org', message_params
    @students_post = StudentsPost.new(students_post_params)
    student = Student.find( @students_post.student_id)
    check = student.students_posts.exists?(@students_post.post_id)
    if student.students_posts.exists?(post_id: @students_post.post_id)
      # @record = "student.name"
      # ModelMailer.new_record_notification(@record).deliver
      flash[:danger] = "You already took this job."
      redirect_to student_students_path
    else
      if @students_post.save
        redirect_to student_students_path, notice: 'Students post was successfully created.'
      else
        render :new
      end
    end
    # if @students_post.save
    #   redirect_to @students_post, notice: 'Students post was successfully created.'
    # else
    #   render :new
    # end
  end

  # PATCH/PUT /students_posts/1
  def update
    if @students_post.update(students_post_params)
      mg_client = Mailgun::Client.new 'key-1f0ceaa138d186ae2a34508c5ea0fbe7'
      message_params =  { from: 'bob@sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org',
                          to:   'torpong.boss@gmail.com',
                          subject: 'Interview from loaclhost',
                          text:    'You have been selected for Interview in xxx company post'
                        }
      mg_client.send_message 'sandbox398f07667a2e474c9c87a8fe345516a9.mailgun.org', message_params
      # redirect_to @students_post, notice: 'Students post was successfully updated.'
      redirect_to students_posts_path(:id => @students_post.id)
    else
      render :edit
    end
  end

  # DELETE /students_posts/1
  def destroy
    @students_post.destroy
    redirect_to students_posts_url, notice: 'Students post was successfully destroyed.'
  end

  # UPDATE /interview_select
  def interview_select
    @interviewers = params[:interviewers]

    if !@interviewers.empty?
      @interviewers.each do |viewer|
        @students_post = StudentsPost.find(viewer)
        @students_post.to_interview!
        StudentsPost.automail
      end
      redirect_to students_posts_path
    end
  end

  def internship_select
    @internshipers = params[:internshipers]

    if !@internshipers.nil?
      @internshipers.each do |internshiper|
        @students_post = StudentsPost.find(internshiper)
        @students_post.to_internship!
        StudentsPost.acceptmail
      end
      redirect_to students_posts_path
    end
  end

  def interview
    @students_post.to_interview!
    StudentsPost.automail
    redirect_to students_post_path(@students_post.id)
  end

  def internship
    @students_post.to_internship!
    StudentsPost.acceptmail
    redirect_to students_post_path(@students_post.id)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_students_post
      @students_post = StudentsPost.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def students_post_params
      params.require(:students_post).permit(:student_id, :post_id, :status)
    end
end
