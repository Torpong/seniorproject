class Company::AppController < ApplicationController
  before_action :authenticate_company!

  def current_user
    current_company
  end

  helper_method :current_user
end
