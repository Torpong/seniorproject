class Company::CompaniesController < Company::AppController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies
  def index
    @companies = Company.all
  end

  # GET /companies/1
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  def create
    @company = Company.new(company_params)

    if @company.save
      flash[:success] ='Company was successfully created.'
      redirect_to @company
    else
      render :new
    end
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params)
      flash[:warning] = 'Company was successfully updated.'
      redirect_to company_company_path(id: @company.id)
    else
      render :edit
    end
  end

  # DELETE /companies/1
  def destroy
    @company.destroy
    flash[:danger] =
    redirect_to companies_url
  end

  def completed
    @completed = Post.where(state: 'completed').paginate(:page => params[:page], :per_page => 5)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def company_params
      params.require(:company).permit(:name, :address)
    end
end
