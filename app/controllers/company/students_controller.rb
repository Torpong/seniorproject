class Company::StudentsController < Company::AppController
  before_action :set_student, only: [:show, :requesting]

  # GET /students/1
  def show
  end

  def requesting
    if @student.no_request?
      @student.to_requested!
      redirect_to company_path(@student.id)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

end
