class Company::PostsController < Company::AppController
  require 'will_paginate/array'
  before_action :set_post, only: [:show, :edit, :update, :destroy, :close, :complete]
  has_scope :starts_with
  def index
      @posts = current_company.post.paginate(:page => params[:page], :per_page => 5)
      @posts = @posts.where.not(state: "completed")
  end

  # GET /posts/1
  def show
    Post.check
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  def create
    @post = Post.new(post_params)
    @post.company = current_company
    @post.publish = true
    if @post.finished_at < Time.now
      raise @post
    end
    if @post.save
      flash[:success] = "Post was successfully created."
      redirect_to company_post_path(@post.id)
    else
      render :new
    end
  end

  # PATCH/PUT /posts/1
  def update

    if @post.update(post_params)
      if @post.publish == nil
        flash[:warning] = 'Post was successfully updated.'
        redirect_to company_post_path(:id => @post.id)
      else
        redirect_to company_post_path(:id => @post.id)
      end
    else
      render :edit
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    flash[:danger] = 'Post was successfully destroyed.'
    redirect_to posts_url
  end

  # PATCH/PUT /posts/1/close
  def close
    @post.to_closed!
    redirect_to company_post_path(@post.id)
  end

  def complete
    @post.to_completed!
    @post.students_posts.each do |student|
      if !student.accepted_interview?
        student.to_reject!
      end
    end
    redirect_to company_post_path(@post.id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title, :state, :jobDescription, :salary,
                                    :skillRequirement, :location, :company_id,
                                     :tag_list, :publish,:finished_at)

    end
end
