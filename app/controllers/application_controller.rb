class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :layout_by_resource

  def current_user
    current_company
  end

  helper_method :current_user
  private

  def layout_by_resource
    if devise_controller?
      "public"
    else
      "application"
    end
  end
end
