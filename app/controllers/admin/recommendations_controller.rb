class Admin::RecommendationsController < Admin::AppController
  before_action :set_recommendation, only: [:show, :edit, :update, :destroy]

  # GET /recommendations
  def index
    @recommendations = Recommendation.all
  end

  # GET /recommendations/1
  def show
    @post = Student.find(params[:id])
    @recommendation = Recommendation.new( :student_id => @student )
  end

  # GET /recommendations/new
  def new
    session[:view_student] = params[:view_student]
    session[:current_user] = params[:current_user]
    @admin = Admin.find(session[:current_user])
    @student = Student.find(session[:view_student])
    @recommendation = Recommendation.new
  end

  # GET /recommendations/1/edit
  def edit
  end

  # POST /recommendations
  def create
    @student = Student.find(session[:view_student])
    @admin = Admin.find(session[:current_user])
    @recommendation = Recommendation.new(recommendation_params)
    @recommendation.student_id = session[:view_student]
    @recommendation.admin_id = session[:current_user]

    if @recommendation.save
      flash[:success] = 'Recommendation was successfully created.'
      redirect_to admin_student_path(@student)
    else
      render :new
    end
  end

  # PATCH/PUT /recommendations/1
  def update
    if @recommendation.update(recommendation_params)
      flash[:warning] = 'Recommendation was successfully updated.'
      redirect_to @recommendation
    else
      render :edit
    end
  end

  # DELETE /recommendations/1
  def destroy
    @recommendation.destroy
    flash[:danger] = 'Recommendation was successfully destroyed.'
    redirect_to admin_recommendations_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recommendation
      @recommendation = Recommendation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def recommendation_params
      params.require(:recommendation).permit(:title, :body, :student_id, :admin_id, :teacher_id)
    end
end
