class Admin::AppController < ApplicationController
  before_action :authenticate_admin!

  def current_user
    current_admin
  end

  helper_method :current_user
end
