class Admin::PostsController < Admin::AppController
  require 'will_paginate/array'
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  has_scope :starts_with
  def index
      @posts = apply_scopes(Post.all).paginate(:page => params[:page], :per_page => 5)
  end


  # GET /posts/1
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  def create
    @post = Post.new(post_params)
    @post.company = current_company
    if @post.save
      flash[:success] = 'Post was successfully created.'
      redirect_to admin_post_path(@post.id)
    else
      render :new
    end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      flash[:warning] =  'Post was successfully updated.'
      redirect_to admin_post_path(@post.id)
    else
      render :edit
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    flash[:danger] = 'Post was successfully destroyed.'
    redirect_to posts_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title, :state, :jobDescription, :salary,
                                    :skillRequirement, :location, :company_id,
                                     :tag_list)
    end
    # def filtering_params(params)
    #   params.slice(:starts_with)
    # end
end
