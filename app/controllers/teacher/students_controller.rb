class Teacher::StudentsController < Teacher::AppController
  before_action :set_student, only: [:request]

  # GET /students
  def index
    @students = Student.all
  end

  # GET /students/new
  def new
    @student = Student.new
  end
  


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

end
