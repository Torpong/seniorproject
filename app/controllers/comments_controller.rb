class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  def show
    @post = Student.find(params[:id])
    @comment = Comment.new( :student_id => @student )
  end

  # GET /comments/new
  def new
    session[:view_student] = params[:view_student]
    session[:current_user] = params[:current_user]
    @company = Company.find(session[:current_user])
    @student = Student.find(session[:view_student])
    @comment = Comment.new
    # raise @student
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  def create
    # raise @student

    # raise comment_params
    # raise params[:view_student]
    # raise comment_params[:student_id]
    # comment_params[:student_id] = @student
    @student = Student.find(session[:view_student])
    @company = Company.find(session[:current_user])
    @comment = Comment.new(comment_params)
    @comment.student_id = session[:view_student]
    @comment.company_id = session[:current_user]
    # raise session[:view_student]
    # raise @comment
    if @comment.save
      redirect_to company_path(@student), notice: 'Comment was successfully created.'
      # redirect_to student_student_path(@student), notice: 'Comment was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      redirect_to @comment, notice: 'Comment was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.destroy
    redirect_to @comment.student, notice: 'Comment was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:name, :body, :student_id, :company_id)
    end
end
