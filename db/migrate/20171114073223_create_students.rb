class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name
      t.integer :age
      t.string :occupation
      t.string :state
      t.timestamps
    end
  end
end
