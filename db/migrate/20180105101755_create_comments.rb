class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :name
      t.string :body
      t.boolean :recommendation
      t.references :student, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
