class CreateTeachers < ActiveRecord::Migration[5.1]
  def change
    create_table :teachers do |t|
      t.string :name
      t.string :age
      t.string :interest
      t.string :major
      t.string :minor

      t.timestamps
    end
  end
end
