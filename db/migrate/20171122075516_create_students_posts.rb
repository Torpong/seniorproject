class CreateStudentsPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :students_posts do |t|
      t.references :student, foreign_key: true
      t.references :post, foreign_key: true
      t.string :status
      t.timestamps
    end
  end
end
