class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :state
      t.string :jobDescription
      t.string :salary
      t.string :skillRequirement
      t.string :location
      t.references :company, foreign_key: true
      t.boolean :publish
      t.datetime :finished_at

      t.timestamps
    end
  end
end
