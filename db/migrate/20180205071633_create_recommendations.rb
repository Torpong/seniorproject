class CreateRecommendations < ActiveRecord::Migration[5.1]
  def change
    create_table :recommendations do |t|
      t.string :title
      t.string :body
      t.references :student, foreign_key: true
      t.references :admin, foreign_key: true
      t.timestamps
    end
  end
end
