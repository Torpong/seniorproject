class AddTeacherToRecommendations < ActiveRecord::Migration[5.1]
  def change
    add_reference :recommendations, :teacher, foreign_key: true
  end
end
