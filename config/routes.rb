Rails.application.routes.draw do

  resources :recommendations
  post '/rate' => 'rater#create', :as => 'rate'
  resources :comments
  resources :students_posts
  put '/students_posts/:id/internship', to: 'students_posts#internship'
  put '/students_posts/:id/choose', to: 'students_posts#choose', :as => 'choose'

  resources :tags, only: [:index, :show]
  put '/bossy/interview_select', to: 'students_posts#interview_select'
  get '/home', to: 'homes#index'

  constraints subdomain: 'teacher' do
    namespace :teacher do
      resources :teachers
      # get '/teachers/request', to: 'teachers#request'
      # get '/students/status', to: 'students#status'
      # resources :students
      # resources :companies
      # resources :recommendations
      resources :recommendations
      resources :students
      resources :posts
    end
    devise_for :teachers, path: 'teachers', controller: { sessions: "teachers/sessions"}
  end


  constraints subdomain: 'admin' do
    namespace :admin do
      resources :admins
      get '/students/status', to: 'students#status'
      resources :students
      resources :companies
      resources :recommendations
      resources :posts
    end
    devise_for :admins, path: 'admins', controller: { sessions: "admins/sessions"}
  end

  constraints subdomain: 'student' do
    # devise_for :students, path: 'students', controller: { sessions: "students/sessions"}
    namespace :student do
      get '/companies/:id', to: 'companies#show'
      get '/students/status', to: 'students#status'
      resources :students
      resources :posts, only: [:index, :show]

    end
    devise_for :students, path: 'students', controller: { sessions: "students/sessions"}
  end

  constraints subdomain: 'company' do


    namespace :company do
      get '/students/:id', to: 'students#show'
      resources :companies
      resources :posts
      put '/students/:id/requesting', to: 'students#requesting' ,as: "student_requesting"
      put '/posts/:id/close', to: 'posts#close' ,as: "post_close"
      put '/posts/:id/complete', to: 'posts#complete' ,as: "post_complete"
      get '/completed', to: 'companies#completed'
      put '/students_posts/:id/interview', to: 'students_posts#interview'
      put '/students_posts/interview_select', to: 'students_posts#interview_select'
      put '/students_posts/internship_select', to: 'students_posts#internship_select'
      put '/students_posts/:id/internship', to: 'students_posts#internship'

    end
    devise_for :companies, path: 'companies', controller: { companies: "companies/sessions"}
  end
  # devise_for :students
  root 'homes#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
