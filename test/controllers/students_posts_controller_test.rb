require 'test_helper'

class StudentsPostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @students_post = students_posts(:one)
  end

  test "should get index" do
    get students_posts_url
    assert_response :success
  end

  test "should get new" do
    get new_students_post_url
    assert_response :success
  end

  test "should create students_post" do
    assert_difference('StudentsPost.count') do
      post students_posts_url, params: { students_post: { post_id: @students_post.post_id, student_id: @students_post.student_id } }
    end

    assert_redirected_to students_post_url(StudentsPost.last)
  end

  test "should show students_post" do
    get students_post_url(@students_post)
    assert_response :success
  end

  test "should get edit" do
    get edit_students_post_url(@students_post)
    assert_response :success
  end

  test "should update students_post" do
    patch students_post_url(@students_post), params: { students_post: { post_id: @students_post.post_id, student_id: @students_post.student_id } }
    assert_redirected_to students_post_url(@students_post)
  end

  test "should destroy students_post" do
    assert_difference('StudentsPost.count', -1) do
      delete students_post_url(@students_post)
    end

    assert_redirected_to students_posts_url
  end
end
